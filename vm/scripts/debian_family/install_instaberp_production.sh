#!/bin/bash -eux

# Install base requirements.
apt-get install -y curl git wget vim python-dev gcc

# Install InstaBERP
wget https://raw.githubusercontent.com/frappe/bench/master/playbooks/install.py
sudo python install.py --production --user frappe --mysql-root-password frappe --admin-password admin